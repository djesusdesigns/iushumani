import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { NewsDetailsPage } from '../pages/news-details/news-details';
import { ViewPdfPage } from '../pages/view-pdf/view-pdf';
import { AboutusPage } from '../pages/aboutus/aboutus';
import {  HelpPage } from '../pages/help/help';
import {  ContactPage } from '../pages/contact/contact';
import { BookPage } from '../pages/book/book';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ApiRestProvider } from '../providers/api-rest/api-rest';
import {TruncatePipe} from '../pipes/truncate/truncate';
import { DocumentViewer } from '@ionic-native/document-viewer';
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    NewsDetailsPage,
    ViewPdfPage,
    BookPage,
    AboutusPage,
    HelpPage,
    ContactPage,
    TruncatePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    PdfViewerModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    NewsDetailsPage,
    ViewPdfPage,
    AboutusPage,
     HelpPage,
     ContactPage,
    BookPage

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiRestProvider,
    DocumentViewer
  ]
})
export class AppModule {}
