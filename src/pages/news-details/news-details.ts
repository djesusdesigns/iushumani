import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,  Platform } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

/**
 * Generated class for the NewsDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var ePub: any;

@IonicPage()
@Component({
  selector: 'page-news-details',
  templateUrl: 'news-details.html',
})
export class NewsDetailsPage {
	public title;
	public descripcion;
  public epub;
  book: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public sanitizer: DomSanitizer,
    public platform: Platform,) {
  	
  	this.title = navParams.get("title")
    this.descripcion = navParams.get("descripcion")
    this.epub = navParams.get("book")

    if (this.navParams.get("book")) {
      let book = this.navParams.get("book");
      this.platform.ready().then(() => {
        // load book
        this.book = ePub(book.file);
        console.log(this.book)

        /*this._updateTotalPages();

        // load toc and then update pagetitle
        this.book.getToc().then(toc => {
          this._updatePageTitle();
        });*/

        // if page changes
        /*this.book.on('book:pageChanged', (location) => {
          console.log('on book:pageChanged', location);
          this._updateCurrentPage();
          this._updatePageTitle();
        });*/

        // subscribe to events coming from other pages
        //this._subscribeToEvents();
      });
    }
    

    /*this.pasos_receta = navParams.get("pasos")
    this.imagen = navParams.get("imagen")
    this.ingredientes = navParams.get("ingredientes")
    this.redactor = navParams.get("redactor")
    this.img_redactor = navParams.get("img_redactor")
    this.id_receta = navParams.get("idreceta")*/
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad NewsDetailsPage');
    if (this.book) {
      this.book.renderTo('book'); // TODO We should work with ready somehow here I think
    }
     
  } //End of ionViewDidLoad() Function

}
