import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RequestexamplerPage } from './requestexampler';

@NgModule({
  declarations: [
    RequestexamplerPage,
  ],
  imports: [
    IonicPageModule.forChild(RequestexamplerPage),
  ],
})
export class RequestexamplerPageModule {}
