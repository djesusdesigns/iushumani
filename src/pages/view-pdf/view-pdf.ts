import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { DomSanitizer } from '@angular/platform-browser';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';

/**
 * Generated class for the ViewPdfPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-view-pdf',
  templateUrl: 'view-pdf.html',
})
export class ViewPdfPage {
	pdf: string = 'https://iushumani.org/files_iush/journals/1/articles/10/public/10-40-2-PBbak.pdf';
	public url;
  pdf2:string = 'assets/10-40-2-PBbak.pdf';
  public url2;
	public title;
	public pdfSrc;
  doc: any;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public sanitizer: DomSanitizer,
    private document: DocumentViewer) {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(navParams.get("urlPDF"));
     this.url2 = this.sanitizer.bypassSecurityTrustResourceUrl(this.pdf);
   
    this.title = navParams.get("title")
    this.pdfSrc ={
  	url: 'https://iushumani.org/files_iush/journals/1/articles/10/public/10-40-2-PBbak.pdf',
  	withCredentials: true
    };
     this.doc =this.sanitizer.bypassSecurityTrustResourceUrl(this.pdfSrc);
  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad ViewPdfPage');
   
  }

  viewPDF(){
    const options: DocumentViewerOptions = {
      title: 'demo'
  }
    this.document.viewDocument('../../assets/10.pdf', 'application/pdf', options)
  }
 

}
