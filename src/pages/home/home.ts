import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
//import { Directive } from '@angular/core';

import {ApiRestProvider} from '../../providers/api-rest/api-rest';
import {TruncatePipe} from '../../pipes/truncate/truncate';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';
import { NewsDetailsPage } from '../news-details/news-details';
import { ViewPdfPage } from '../view-pdf/view-pdf';
import { BookPage } from '../book/book';

export class Book {
  label: string;
  file: string;
}

declare var ePub: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})


export class HomePage {
	
  pet: string = "news";
  newsData: any;
  volData: any; 
  searchData: any; 
  noFindSearchData: any;
  loadingSpiner = 'loading';
  loadingsearch: any;
  loadingVol: any;
  textSearch: any;
  books: {}[];
  ePub:any;
  
  constructor(
    public navCtrl: NavController,
    private api: ApiRestProvider,
    private document: DocumentViewer) {

  	this.showNews();
    this.books = [];

    let book2 = new Book();
    book2.label = "Vol 7 (.epub)";
    book2.file =  "assets/books/iushumani-vol-7.epub";
    this.books.push(book2);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookPage');
 }

  showNews(){
  	this.api.getArticles()
    .subscribe(data =>  {
       this.newsData = data['issues'];
       this.loadingSpiner ="stop"
       //console.log(this.newsData)
    });
  }

  showIssuesVol(){
     this.loadingVol = 'loading';
  	this.api.getIssuesVol()
    .subscribe(data =>  {
       this.volData = data['issues'];
        this.loadingVol = 'noloading';
       //const articlesLength = this.volData.map(x => x.publisehd_articles);
       //console.log(articlesLength.length)
    
  }, ERROR => {
      console.error('ERROR', ERROR);
  });
  }

  //
  // SEARCH FUNCTION
  //
  search(ev: any){
    let val = ev.target.value;
    this.textSearch = ev.target.value;
    //let load = 'loading';
    this.loadingsearch = 'loading';

      if (val && val.trim() != '') {

        this.api.searchIssues('search_submission', val, 'isuhumani')
        .subscribe(data =>  {

            if (data['articles']) {
              this.searchData = data['articles'];
              // console.log(Array.of(data))
            } else {
              this.searchData = null;
            }//END if data['articles']

             if (data['message']) {

              this.noFindSearchData = Array.of(data);
              //console.log(Array.of(data))
            } else {
              this.noFindSearchData = null;
            } //END if data['message']

          this.loadingsearch = 'noloading';
          //console.log(this.searchData)
        }, ERROR => {
          console.error('ERROR', ERROR);
      });

      } else {
        val = null;
         this.loadingsearch = 'noloading';
          this.searchData = null;
          this.noFindSearchData = null;
      } //END OF If val search is null
  } //END OF search FUNCTION

  /*-------------------- DETAILS FUNCTION ---------------------*/


  newsDetails(news){
    this.navCtrl.push(NewsDetailsPage , {
      title: news.title,
      descripcion: news.description,
      epub: news.urlEPUB
    })
  } //END of NEWS DETAILS

  searchDetails(search){
    this.navCtrl.push(NewsDetailsPage , {
      title: search.name,
      descripcion: search.citations,
    })
  } //END of NEWS DETAILS

  showEpub(book) {
    //console.log('show', book);
    this.navCtrl.push(BookPage, {
      book: book
    });
  }

  newsViewPdf(news){
    this.navCtrl.push(ViewPdfPage , {
      urlPDF: news.urlPDF,
      title: news.title
    })
  } //END of NEWS DETAILS

  viewPDF(){
    const options: DocumentViewerOptions = {
      title: 'Freddy'
  }
    this.document.viewDocument('../../assets/10.pdf', 'application/pdf', options)
  }

}
